![alt text](https://img.shields.io/badge/version-1.0.0-green 'Version')
[![Netlify Status](https://api.netlify.com/api/v1/badges/8c1fa7f3-6455-4b10-a210-de3fe2ce1388/deploy-status)](https://app.netlify.com/sites/sylwester/deploys)

# Sylwester

Sylwester is the photography portfolio of Sebastian Vuye.

This project was bootstrapped with <br />
[Create React App](https://github.com/facebook/create-react-app) <br />
[Tailwindcss](https://tailwindcss.com/)

## Installation

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />

### `yarn build`

Builds the app for production to the `build` folder.<br />

### `yarn eject`

## Usage

When you cloned the repository you need to remove .git/hooks directory because Git LFS creates some hooks by default and husky does not have an overwrite option yet.

```bash
$ rm -rf .git/hooks
$ yarn install

```
