import React, { useEffect, useState } from 'react';
import { BrowserRouter } from 'react-router-dom';

import { ThemeProvider, Toggle, Footer, Header, Main } from './components';
import { BREAKPOINT } from './constants';
import debounce from './utils/debounce';

const App = () => {
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);

  useEffect(() => {
    const updateWindowWidth = debounce(() => setWindowWidth(window.innerWidth), 200);
    window.addEventListener('resize', updateWindowWidth);
    return () => window.removeEventListener('resize', updateWindowWidth);
  });

  return (
    <BrowserRouter>
      <ThemeProvider>
        <div className='content h-auto md:h-full relative w-full'>
          {windowWidth >= BREAKPOINT.LG && <Toggle windowWidth={windowWidth} />}
          <Header windowWidth={windowWidth} />
          <Main windowWidth={windowWidth} />
          <Footer />
        </div>
      </ThemeProvider>
    </BrowserRouter>
  );
};

export default App;
