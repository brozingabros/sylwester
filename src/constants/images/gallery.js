import { heroImages, homeImages, aboutImages, twelveDaysInMontenegroImages } from './images';
import { configuration } from './configuration';

export const heroGallery = {
  desktop: {
    alt: 'Gas station at night',
    src: heroImages[0],
    srcSize: '3480',
    height: 1947,
    width: 3840,
    ...configuration.heroImage
  },
  mobile: {
    alt: 'Gas station at night',
    id: 0,
    src: heroImages[1],
    srcSize: '1100',
    lazyload: { desktop: false, tablet: false, mobile: false },
    height: 550,
    width: 1100,
    ...configuration.homeGallery
  }
};
export const homeGallery = {
  left: [
    { ...heroGallery.mobile },
    {
      alt: 'A silhouette of a woman in a dark tunnel',
      id: 1,
      src: homeImages[0],
      srcSize: '1100',
      lazyload: { desktop: false, tablet: false, mobile: false },
      height: 732,
      width: 1100,
      ...configuration.homeGallery
    },
    {
      alt: 'A sun flare between branches',
      id: 2,
      src: homeImages[1],
      srcSize: '1100',
      lazyload: { desktop: true, tablet: true, mobile: true },
      height: 469,
      width: 1100,
      ...configuration.homeGallery
    },
    {
      alt: 'A man drinking beer',
      id: 3,
      src: homeImages[2],
      srcSize: '1100',
      lazyload: { desktop: true, tablet: true, mobile: true },
      height: 732,
      width: 1100,
      ...configuration.homeGallery
    },
    {
      alt: 'Tram 12 waiting at night',
      id: 4,
      src: homeImages[7],
      srcSize: '1100',
      lazyload: { desktop: true, tablet: true, mobile: true },
      height: 1648,
      width: 1100,
      ...configuration.homeGallery
    }
  ],
  center: [
    {
      alt: 'A view of Piran in Slovenia',
      id: 5,
      src: homeImages[4],
      srcSize: '1100',
      lazyload: { desktop: false, tablet: true, mobile: true },
      height: 1648,
      width: 1100,
      ...configuration.homeGallery
    },
    {
      alt: 'A portrait of 3 guys in the screen facing the camera',
      id: 6,
      src: homeImages[5],
      srcSize: '1100',
      lazyload: { desktop: true, tablet: true, mobile: true },
      height: 732,
      width: 1100,
      ...configuration.homeGallery
    },
    {
      alt: 'A woman at night looking up between leaves of a tree',
      id: 7,
      src: homeImages[6],
      srcSize: '1100',
      lazyload: { desktop: true, tablet: true, mobile: true },
      height: 469,
      width: 1100,
      ...configuration.homeGallery
    },
    {
      alt: 'A man with a pink cowboy had',
      id: 8,
      src: homeImages[3],
      srcSize: '1100',
      lazyload: { desktop: true, tablet: true, mobile: true },
      height: 732,
      width: 1100,
      ...configuration.homeGallery
    }
  ],
  right: [
    {
      alt: 'A man looking up into the light at night',
      id: 9,
      src: homeImages[8],
      srcSize: '1100',
      lazyload: { desktop: false, tablet: false, mobile: true },
      height: 1648,
      width: 1100,
      ...configuration.homeGallery
    },
    {
      alt: 'A man waring sunglasses staring',
      id: 10,
      src: homeImages[9],
      srcSize: '1100',
      lazyload: { desktop: true, tablet: true, mobile: true },
      height: 732,
      width: 1100,
      ...configuration.homeGallery
    },
    {
      alt: 'A abandoned camper covered in snow in a forrest',
      id: 11,
      src: homeImages[10],
      srcSize: '1100',
      lazyload: { desktop: true, tablet: true, mobile: true },
      height: 469,
      width: 1100,
      ...configuration.homeGallery
    },
    {
      alt: 'A tub in a snowy field',
      id: 12,
      src: homeImages[11],
      srcSize: '1100',
      lazyload: { desktop: true, tablet: true, mobile: true },
      height: 469,
      width: 1100,
      ...configuration.homeGallery
    }
  ]
};
export const aboutGallery = {
  alt: 'A silhouette of a woman in a dark tunnel',
  id: 1,
  src: aboutImages,
  srcSize: '2048',
  lazyload: { desktop: false, tablet: false, mobile: false },
  height: 1046,
  width: 2048,
  ...configuration.aboutImage
};
export const twelveDaysInMontenegroGallery = {
  thumbnail: {
    alt: 'Thumnail',
    id: 0,
    src: twelveDaysInMontenegroImages[0],
    lazyload: { desktop: false, tablet: false, mobile: false },
    height: 952,
    width: 1422,
    ...configuration.projectGallery
  },
  gallery: [
    {
      alt: 'Jep',
      id: 1,
      src: twelveDaysInMontenegroImages[1],
      size: 'full',
      lazyload: { desktop: false, tablet: false, mobile: false },
      height: 2069,
      width: 3084,
      ...configuration.projectDetailGallery.full
    },
    {
      alt: 'Wot',
      id: 2,
      src: twelveDaysInMontenegroImages[2],
      size: 'half',
      lazyload: { desktop: true, tablet: true, mobile: true },
      height: 2159,
      width: 1440,
      ...configuration.projectDetailGallery.half
    },
    {
      alt: 'Wot',
      id: 3,
      src: twelveDaysInMontenegroImages[3],
      size: 'half',
      lazyload: { desktop: true, tablet: true, mobile: true },
      height: 2159,
      width: 1440,
      ...configuration.projectDetailGallery.half
    },
    {
      alt: 'Wot',
      id: 4,
      src: twelveDaysInMontenegroImages[4],
      size: 'full',
      lazyload: { desktop: true, tablet: true, mobile: true },
      height: 2069,
      width: 3084,
      ...configuration.projectDetailGallery.full
    },
    {
      alt: 'Wot',
      id: 5,
      src: twelveDaysInMontenegroImages[5],
      size: 'half',
      lazyload: { desktop: true, tablet: true, mobile: true },
      height: 2159,
      width: 1440,
      ...configuration.projectDetailGallery.half
    },
    {
      alt: 'Wot',
      id: 6,
      src: twelveDaysInMontenegroImages[6],
      size: 'half',
      lazyload: { desktop: true, tablet: true, mobile: true },
      height: 2159,
      width: 1440,
      ...configuration.projectDetailGallery.half
    }
  ]
};
