// IMPORTS

// hero image
import heroImage from '../../assets/hero_home.jpg';
import heroImageMobile from '../../assets/hero_home_mobile.jpg';

// home images
import homeImage01 from '../../assets/LiesbethTunnel.jpg';
import homeImage02 from '../../assets/TreeSun.jpg';
import homeImage03 from '../../assets/ToonDrinking.jpg';
import homeImage04 from '../../assets/ArthurHat.jpg';
import homeImage05 from '../../assets/PiranView.jpg';
import homeImage06 from '../../assets/LinoAndCo.jpg';
import homeImage07 from '../../assets/LiesbethFalll.jpg';
import homeImage08 from '../../assets/Tram12.jpg';
import homeImage09 from '../../assets/MaximLookingUp.jpg';
import homeImage10 from '../../assets/MaximZen.jpg';
import homeImage11 from '../../assets/CamperSnow.jpg';
import homeImage12 from '../../assets/TubSnow.jpg';

// about images
import aboutImage01 from '../../assets/about.jpg';

// 12 days in montenegro
import twelveDaysInMontenegroImageThumbnail from '../../assets/projects/12DaysInMontenegro/12DaysInMontenegroThumbnail.jpg';
import twelveDaysInMontenegroImage01 from '../../assets/projects/12DaysInMontenegro/12DaysInMontenegro_01.jpg';
import twelveDaysInMontenegroImage02 from '../../assets/projects/12DaysInMontenegro/12DaysInMontenegro_02.jpg';
import twelveDaysInMontenegroImage03 from '../../assets/projects/12DaysInMontenegro/12DaysInMontenegro_03.jpg';
import twelveDaysInMontenegroImage04 from '../../assets/projects/12DaysInMontenegro/12DaysInMontenegro_04.jpg';
import twelveDaysInMontenegroImage05 from '../../assets/projects/12DaysInMontenegro/12DaysInMontenegro_05.jpg';
import twelveDaysInMontenegroImage06 from '../../assets/projects/12DaysInMontenegro/12DaysInMontenegro_06.jpg';

// EXPORTS
export const heroImages = [heroImage, heroImageMobile];
export const homeImages = [
  homeImage01,
  homeImage02,
  homeImage03,
  homeImage04,
  homeImage05,
  homeImage06,
  homeImage07,
  homeImage08,
  homeImage09,
  homeImage10,
  homeImage11,
  homeImage12
];
export const twelveDaysInMontenegroImages = [
  twelveDaysInMontenegroImageThumbnail,
  twelveDaysInMontenegroImage01,
  twelveDaysInMontenegroImage02,
  twelveDaysInMontenegroImage03,
  twelveDaysInMontenegroImage04,
  twelveDaysInMontenegroImage05,
  twelveDaysInMontenegroImage06
];
export const aboutImages = aboutImage01;
