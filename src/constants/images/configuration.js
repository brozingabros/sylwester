export const configuration = {
  aboutImage: {
    sizes: '(min-width: 640px) 75vw, calc(91.56vw - 18px)',
    srcSet: [
      // 320px screen resolution
      {
        size: 275,
        width: 275
      },
      // 360px screen resolution
      {
        size: 312,
        width: 312
      },
      // 375px screen resolution
      {
        size: 326,
        width: 326
      },
      // 414px screen resolution
      {
        size: 362,
        width: 362
      },
      // 600px screen resolution
      {
        size: 532,
        width: 532
      },
      // 768px screen resolution
      {
        size: 558,
        width: 558
      },
      // 800px screen resolution
      {
        size: 582,
        width: 582
      },
      // 1024px screen resolution
      {
        size: 750,
        width: 750
      },
      // 1280px screen resolution
      {
        size: 942,
        width: 942
      },
      // 1366px screen resolution
      {
        size: 1007,
        width: 1007
      },
      // 1440px screen resolution
      {
        size: 1062,
        width: 1062
      },
      // 1536px screen resolution
      {
        size: 1134,
        width: 1134
      },
      // 1600px screen resolution
      {
        size: 1182,
        width: 1182
      },
      // 1920px screen resolution
      {
        size: 1422,
        width: 1422
      },
      // 2560px screen resolution
      {
        size: 1902,
        width: 1902
      }
    ]
  },
  heroImage: {
    sizes: '100vw',
    srcSet: [
      // 320px screen resolution
      {
        size: 320,
        width: 320
      },
      // 360px screen resolution
      {
        size: 360,
        width: 360
      },
      // 375px screen resolution
      {
        size: 375,
        width: 375
      },
      // 414px screen resolution
      {
        size: 414,
        width: 414
      },
      // 600px screen resolution
      {
        size: 600,
        width: 600
      },
      // 768px screen resolution
      {
        size: 768,
        width: 768
      },
      // 800px screen resolution
      {
        size: 800,
        width: 800
      },
      // 1024px screen resolution
      {
        size: 1024,
        width: 1024
      },
      // 1280px screen resolution
      {
        size: 1280,
        width: 1280
      },
      // 1366px screen resolution
      {
        size: 1366,
        width: 1366
      },
      // 1440px screen resolution
      {
        size: 1440,
        width: 1440
      },
      // 1536px screen resolution
      {
        size: 1536,
        width: 1536
      },
      // 1600px screen resolution
      {
        size: 1600,
        width: 1600
      },
      // 1920px screen resolution
      {
        size: 1920,
        width: 1920
      },
      // 2560px screen resolution
      {
        size: 2560,
        width: 2560
      },
      // 3840px screen resolution
      {
        size: 3840,
        width: 3840
      }
    ]
  },
  homeGallery: {
    sizes:
      '(min-width: 1280px) calc(24.77vw - 18px), (min-width: 1040px) calc(37.73vw - 20px), (min-width: 780px) 45.83vw, calc(91.74vw - 18px)',
    srcSet: [
      // 320px screen resolution
      {
        size: 294,
        width: 294
      },
      // 360px screen resolution
      {
        size: 330,
        width: 330
      },
      // 375px screen resolution
      {
        size: 344,
        width: 344
      },
      // 414px screen resolution
      {
        size: 380,
        width: 380
      },
      // 600px screen resolution
      {
        size: 550,
        width: 550
      },
      // 2560px screen resolution
      {
        size: 634,
        width: 634
      },
      // 3840px screen resolution
      {
        size: 960,
        width: 960
      }
    ]
  },
  projectGallery: {
    sizes: '(min-width: 1040px) 37.5vw, (min-width: 780px) 45.83vw, calc(91.74vw - 18px)',
    srcSet: [
      // 320px screen resolution
      {
        size: 275,
        width: 275
      },
      // 360px screen resolution
      {
        size: 312,
        width: 312
      },
      // 375px screen resolution
      {
        size: 326,
        width: 326
      },
      // 414px screen resolution
      {
        size: 362,
        width: 362
      },
      // 600px screen resolution
      {
        size: 532,
        width: 532
      },
      // 1536px screen resolution
      {
        size: 558,
        width: 558
      },
      // 1600px screen resolution
      {
        size: 582,
        width: 582
      },
      // 1920px screen resolution
      {
        size: 702,
        width: 702
      },
      // 2560px screen resolution
      {
        size: 942,
        width: 942
      },
      // 3840px screen resolution
      {
        size: 1422,
        width: 1422
      }
    ]
  },

  projectDetailGallery: {
    full: {
      sizes: '(min-width: 1040px) 75vw, calc(91.67vw - 18px)',
      srcSet: [
        // 320px screen resolution
        {
          size: 294,
          width: 294
        },
        // 360px screen resolution
        {
          size: 330,
          width: 330
        },
        // 375px screen resolution
        {
          size: 344,
          width: 344
        },
        // 414px screen resolution
        {
          size: 380,
          width: 380
        },
        // 600px screen resolution
        {
          size: 550,
          width: 550
        },
        // 768px screen resolution
        {
          size: 704,
          width: 704
        },
        // 800px screen resolution
        {
          size: 734,
          width: 734
        },
        // 1024px screen resolution
        {
          size: 768,
          width: 768
        },
        // 1280px screen resolution
        {
          size: 960,
          width: 960
        },
        // 1366px screen resolution
        {
          size: 1025,
          width: 1025
        },
        // 1440px screen resolution
        {
          size: 1080,
          width: 1080
        },
        // 1536px screen resolution
        {
          size: 1152,
          width: 1152
        },
        // 1600px screen resolution
        {
          size: 1200,
          width: 1200
        },
        // 1920px screen resolution
        {
          size: 1440,
          width: 1440
        },
        // 2560px screen resolution
        {
          size: 1542,
          width: 1542
        },
        // 3840px screen resolution
        {
          size: 2880,
          width: 2880
        }
      ]
    },
    half: {
      sizes: '(min-width: 1040px) 37.5vw, (min-width: 640px) calc(46.05vw - 20px), calc(91.56vw - 18px)',
      srcSet: [
        // 320px screen resolution
        {
          size: 294,
          width: 294
        },
        // 360px screen resolution
        {
          size: 330,
          width: 330
        },
        // 375px screen resolution
        {
          size: 344,
          width: 344
        },
        // 414px screen resolution
        {
          size: 380,
          width: 380
        },
        // 600px screen resolution
        {
          size: 550,
          width: 550
        },
        // 1536px screen resolution
        {
          size: 576,
          width: 576
        },
        // 1600px screen resolution
        {
          size: 600,
          width: 600
        },
        // 1920px screen resolution
        {
          size: 720,
          width: 720
        },
        // 2560px screen resolution
        {
          size: 960,
          width: 960
        },
        // 3840px screen resolution
        {
          size: 1440,
          width: 1440
        }
      ]
    }
  }
};
