import React, { lazy } from 'react';
import projectData from './projects';

const Home = lazy(() => import('../components/Home/Home'));
const Projects = lazy(() => import('../components/Projects/Projects'));
const About = lazy(() => import('../components/About/index'));
const Contact = lazy(() => import('../components/Contact/index'));
const NotFound = lazy(() => import('../components/404/index'));

const routes = [
  // eslint-disable-next-line react/prop-types
  { component: ({ windowWidth }) => <Home title='Home' windowWidth={windowWidth} />, exact: true, id: 1, path: '/' },
  { component: () => <Projects title='Projects' />, exact: true, id: 2, path: '/projects' },
  { component: () => <About title='About' />, exact: true, id: 3, path: '/about' },
  { component: () => <Contact title='Contact' />, exact: true, id: 4, path: '/contact' },
  ...projectData,
  { component: () => <NotFound title='Not Found' />, id: 4 }
];

export default routes;
