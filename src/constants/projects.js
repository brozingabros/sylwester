import React, { lazy } from 'react';
import { twelveDaysInMontenegroGallery } from '.';

const ProjectDetail = lazy(() => import('../components/ProjectDetail/ProjectDetail'));
const path = '/projects/:id';

const projectData = [
  {
    description: `Travelling through Montenegro felt like using a time machine to go back to my childhood, growing up in rural Poland. The nostalgia to a careless moment where time passed by much slower and the daily rush we experience in the West did not exist.`,
    img: { ...twelveDaysInMontenegroGallery.thumbnail },
    tagline: 'DISCOVERING UNCHARTED BEAUTY IN THE BALKAN',
    title: '12 Days in Montenegro',
    // eslint-disable-next-line react/prop-types
    component: ({ windowWidth }) => (
      <ProjectDetail
        imageSource={twelveDaysInMontenegroGallery.gallery}
        title='12 Days in Montenegro'
        windowWidth={windowWidth}>
        {projectData[0].title}
      </ProjectDetail>
    ),
    exact: true,
    id: 10,
    url: '12-days-in-montenegro',
    path
  }
];

export default projectData;
