import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import Locky from 'react-locky';
import { BREAKPOINT } from '../../constants';
import { Hamburger } from '../Hamburger';
import { Toggle } from '../Toggle';

const menuConfiguration = {
  left: [
    { id: 1, path: '/', text: 'Home' },
    { id: 2, path: '/projects', text: 'Projects' }
  ],
  right: [
    { id: 3, path: '/about', text: 'About' },
    { id: 4, path: '/contact', text: 'Contact' }
  ]
};

const MenuItem = ({ mobile, children, onClick, path }) => {
  return (
    <li
      className={
        mobile
          ? 'font-bold my-2 text-2xl text-white nav-item-mobile'
          : 'font-bold leading-none mx-4 text-base text-primary'
      }>
      {mobile ? (
        <NavLink to={path} onClick={() => onClick(false)}>
          {children}
        </NavLink>
      ) : (
        <NavLink to={path}>{children}</NavLink>
      )}
    </li>
  );
};

const MenuDesktop = () => {
  return Object.keys(menuConfiguration).map(navigation => {
    return (
      <nav className={navigation === 'left' ? 'order-1' : 'order-3'} key={`${navigation}-key`}>
        <ul className='flex font-body'>
          {navigation === 'left'
            ? menuConfiguration.left.map(menuItem => (
                <MenuItem key={menuItem.id} path={menuItem.path}>
                  {menuItem.text}
                </MenuItem>
              ))
            : menuConfiguration.right.map(menuItem => (
                <MenuItem key={menuItem.id} path={menuItem.path}>
                  {menuItem.text}
                </MenuItem>
              ))}
        </ul>
      </nav>
    );
  });
};
const MenuMobile = ({ clickState, onClick }) => {
  return (
    <nav
      className={
        clickState
          ? 'absolute bottom-0 bg-secondary duration-500 ease-in-out h-full right-0 top-0 transition-all w-full z-10 nav-mobile-wrapper'
          : 'bg-secondary bottom-0 duration-500 ease-in-out fixed h-full right-0 top-0 transform transition--all translate-x-full w-full z-10 nav-mobile-wrapper'
      }>
      <ul className='flex items-center w-full nav-mobile'>
        {[...menuConfiguration.left, ...menuConfiguration.right].map(menuItem => (
          <MenuItem mobile key={menuItem.id} path={menuItem.path} onClick={onClick}>
            {menuItem.text}
          </MenuItem>
        ))}
      </ul>
      <Toggle />
    </nav>
  );
};

const Menu = ({ windowWidth }) => {
  const [hamburgerClickState, setHamburgerClickState] = useState(false);
  const handleHamburgerClick = () =>
    hamburgerClickState ? setHamburgerClickState(false) : setHamburgerClickState(true);

  return (
    <>
      {windowWidth >= BREAKPOINT.LG ? (
        <MenuDesktop />
      ) : (
        <>
          <Locky enabled={hamburgerClickState}>
            <Hamburger clickState={hamburgerClickState} onClick={handleHamburgerClick} />
            <MenuMobile clickState={hamburgerClickState} onClick={handleHamburgerClick} />
          </Locky>
        </>
      )}
    </>
  );
};

export default Menu;

Menu.defaultProps = {
  windowWidth: BREAKPOINT.LG
};
Menu.propTypes = {
  windowWidth: PropTypes.number
};

MenuItem.defaultProps = {
  children: '',
  mobile: false,
  path: '',
  onClick: () => false
};

MenuItem.propTypes = {
  children: PropTypes.node,
  mobile: PropTypes.bool,
  path: PropTypes.string,
  onClick: PropTypes.func
};

MenuMobile.defaultProps = {
  clickState: false,
  onClick: () => false
};
MenuMobile.propTypes = {
  clickState: PropTypes.bool,
  onClick: PropTypes.func
};
