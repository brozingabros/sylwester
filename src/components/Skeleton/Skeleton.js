import React from 'react';
import ContentLoader from 'react-content-loader';

const TempSkeleton = () => (
  <ContentLoader
    backgroundColor='#dedede'
    className='mt-8'
    foregroundColor='#ecebeb'
    height={1925}
    speed={5}
    viewBox='0 0 3840 1925'
    width={3840}>
    <rect height='1925' width='3840' x='0' y='0' />
  </ContentLoader>
);

export default TempSkeleton;
