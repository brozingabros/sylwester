import React from 'react';
import PropTypes from 'prop-types';
import { Hero } from '../Hero';
import { Gallery } from '../Gallery';
import { BREAKPOINT, heroGallery, homeGallery } from '../../constants';

const Home = ({ windowWidth, title }) => {
  document.title = title;
  return (
    <>
      {windowWidth >= BREAKPOINT.MD && <Hero source={heroGallery.desktop} />}
      <Gallery source={homeGallery} windowWidth={windowWidth} />
    </>
  );
};

export default Home;

Home.defaultProps = {
  title: 'Title',
  windowWidth: 0
};
Home.propTypes = {
  title: PropTypes.string,
  windowWidth: PropTypes.number
};
