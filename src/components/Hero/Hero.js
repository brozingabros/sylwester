import React from 'react';
import PropTypes from 'prop-types';
import { HeroImage } from '../Image';

const Hero = ({ source }) => {
  return (
    <section className='my-8'>
      <HeroImage source={source} wrapperElement='section' />
    </section>
  );
};

export default Hero;

Hero.defaultProps = {
  source: PropTypes.shape({
    src: '',
    alt: '',
    height: 0,
    width: 0
  })
};
Hero.propTypes = {
  source: PropTypes.shape({
    src: PropTypes.string,
    alt: PropTypes.string,
    height: PropTypes.number,
    width: PropTypes.number
  })
};
