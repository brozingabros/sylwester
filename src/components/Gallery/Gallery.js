import React from 'react';
import PropTypes from 'prop-types';
import { BREAKPOINT } from '../../constants';
import { GalleryImage } from '../Image';

const Gallery = ({ source, windowWidth }) => {
  const Columns = () => {
    const splitHalf = Math.floor(source.center.length / 2);

    if (windowWidth >= BREAKPOINT.XL) {
      return Object.keys(source).map(column => (
        <ul className='flex-100 justify-center md:flex-50  xl:flex-33' key={column}>
          {column === 'left' &&
            source.left
              .slice(1)
              .map(image => (
                <GalleryImage key={image.id} lazyloadTarget='desktop' source={image} wrapperElement='li' />
              ))}
          {column === 'center' &&
            source.center.map(image => (
              <GalleryImage key={image.id} lazyloadTarget='desktop' source={image} wrapperElement='li' />
            ))}
          {column === 'right' &&
            source.right.map(image => (
              <GalleryImage key={image.id} lazyloadTarget='desktop' source={image} wrapperElement='li' />
            ))}
        </ul>
      ));
    }

    if (windowWidth < BREAKPOINT.MD) {
      return Object.keys(source).map(column => (
        <ul className='flex-100 justify-center md:flex-50  xl:flex-33' key={column}>
          {column === 'left' &&
            source.left.map(image => (
              <GalleryImage key={image.id} lazyloadTarget='mobile' source={image} wrapperElement='li' />
            ))}
          {column === 'center' &&
            source.center.map(image => (
              <GalleryImage key={image.id} lazyloadTarget='mobile' source={image} wrapperElement='li' />
            ))}
          {column === 'right' &&
            source.right.map(image => (
              <GalleryImage key={image.id} lazyloadTarget='mobile' source={image} wrapperElement='li' />
            ))}
        </ul>
      ));
    }

    return (
      <>
        <ul className='justify-center md:flex-50 w-full'>
          {[...source.left.slice(1), ...source.center.slice(0, splitHalf)].map(image => (
            <GalleryImage key={image.id} lazyloadTarget='tablet' source={image} wrapperElement='li' />
          ))}
        </ul>
        <ul className='justify-center md:flex-50 w-full'>
          {[...source.right, ...source.center.slice(splitHalf)].map(image => (
            <GalleryImage key={image.id} lazyloadTarget='tablet' source={image} wrapperElement='li' />
          ))}
        </ul>
      </>
    );
  };

  return (
    <section className='flex flex-wrap lg:w-9/12 md:mt-0 mt-8 mx-auto w-11/12'>
      <Columns />
    </section>
  );
};

export default Gallery;

Gallery.defaultProps = {
  source: {},
  windowWidth: 0
};

Gallery.propTypes = {
  source: PropTypes.shape({
    left: PropTypes.arrayOf(
      PropTypes.shape({
        alt: PropTypes.string,
        id: PropTypes.number,
        src: PropTypes.string
      })
    ),
    center: PropTypes.arrayOf(
      PropTypes.shape({
        alt: PropTypes.string,
        id: PropTypes.number,
        src: PropTypes.string
      })
    ),
    right: PropTypes.arrayOf(
      PropTypes.shape({
        alt: PropTypes.string,
        id: PropTypes.number,
        src: PropTypes.string
      })
    )
  }),
  windowWidth: PropTypes.number
};
