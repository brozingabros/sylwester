import React from 'react';

const Footer = () => {
  const currentDate = new Date();

  return (
    <footer className='bg-secondary flex justify-center mt-16 py-4 w-full'>
      <section>
        <p className='font-body font-bold md:text-sm text-center text-secondary text-xs uppercase'>
          &copy; Sylwester &nbsp;
          <span>{currentDate.getFullYear()}</span>
        </p>
      </section>
    </footer>
  );
};

export default Footer;
