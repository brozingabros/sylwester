import React from 'react';
import PropTypes from 'prop-types';
import { BREAKPOINT } from '../../constants';
import { Menu } from '../Menu';
import { Logo } from '../Logo';

const Header = ({ windowWidth }) => {
  return (
    <header
      className={
        windowWidth < BREAKPOINT.LG
          ? 'flex items-center justify-between pt-5 mx-auto w-11/12'
          : 'flex items-center justify-center pt-10 mx-auto w-full'
      }>
      <Logo />
      <Menu windowWidth={windowWidth} />
    </header>
  );
};

export default Header;

Header.defaultProps = {
  windowWidth: 0
};
Header.propTypes = {
  windowWidth: PropTypes.number
};
