import React from 'react';
import PropTypes from 'prop-types';
import { Title } from '../Title';
import { ProjectGallery } from '../ProjectGallery';

const Projects = ({ windowWidth, title }) => {
  document.title = title;
  return (
    <section className='flex flex-wrap lg:w-9/12 md:mt-0 mt-8 mx-auto w-11/12'>
      <Title>Projects</Title>
      <ProjectGallery windowWidth={windowWidth} />
    </section>
  );
};

export default Projects;

Projects.defaultProps = {
  title: 'Title',
  windowWidth: 0
};
Projects.propTypes = {
  title: PropTypes.string,
  windowWidth: PropTypes.number
};
