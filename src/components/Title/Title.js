import React from 'react';
import PropTypes from 'prop-types';

const Title = ({ children }) => {
  return (
    <section className='flex justify-center my-16 w-full'>
      <h2 className='font-primary leading-none text-lg text-primary uppercase'>{children}</h2>
    </section>
  );
};

Title.defaultProps = {
  children: 'Site Title'
};
Title.propTypes = {
  children: PropTypes.string
};

export default Title;
