import React from 'react';
import PropTypes from 'prop-types';
import { Title } from '../Title';
import { HeroImage } from '../Image';
import { aboutGallery } from '../../constants/images/gallery';

const About = ({ title }) => {
  document.title = title;
  return (
    <section className='flex flex-wrap lg:w-9/12 md:mt-0 mt-8 mx-auto w-11/12'>
      <Title>About</Title>
      <section className='flex-col sm:flex-row w-full'>
        <section className='p-2 w-full'>
          <HeroImage source={aboutGallery} wrapperElement='section' />
        </section>
        <section className='p-4 w-full'>
          <h3 className='font-primary leading-none text-lg text-primary uppercase'>Hi, my name is Seba</h3>
          <h4 className='font-secondary leading-none mt-2 text-md text-primary uppercase'>SEBASTIAN SYLWESTER VUYE</h4>
          <p className='font-body leading-7 mt-8 text-base text-primary'>
            I’m a photographer based in Antwerp, Belgium.
          </p>
          <p className='font-body leading-7 mt-8 text-base text-primary'>
            I graduated from the film school Narafi and have utilized the skills acquired during the education on
            diverse film and television sets. Afterwards, I directed and shot several award winning short films. Also,
            I’ve worked with the camera department on projects for clients such as Woestijnvis, Miss Fashion, Geronimo
            and Exxon. In 2015, my short ‘I Have a Rendezvous with Death’ was selected as one of nine nominees during
            the Future Talent’s Awards in Belgium.
          </p>
          <p className='font-body leading-7 mt-8 text-base text-primary'>
            Currently, I mainly focus on photography but I incorporate my love of cinema into my work as a photographer.
            I am fascinated by crafting beautiful and meaningful images in service of telling great stories.
          </p>
          <p className='font-body leading-7 mt-8 text-base text-primary'>
            My favorite scene is the night train scene in the film ‘The Assassination of Jesse James by the Coward
            Robert Ford’, which was shot by Roger Deakins.
          </p>
        </section>
      </section>
    </section>
  );
};

export default About;

About.defaultProps = {
  title: 'Title'
};
About.propTypes = {
  title: PropTypes.string
};
