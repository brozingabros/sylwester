import React from 'react';
import PropTypes from 'prop-types';

const NotFound = ({ title }) => {
  document.title = title;
  return (
    <section className='items-center flex h-full justify-center lg:w-9/12 mx-auto w-11/12'>
      <section>
        <h2 className='font-body leading-10 text-6xl text-center'>404</h2>
        <p className='font-secondary text-sm text-center'>Page not found</p>
        <p className='font-body mt-8 mx-auto sm:w-6/12 text-xs text-center w-full'>
          The page you are looking for might have been removed, has it&apos;s name changed or is temporarily
          unavailable.
        </p>
      </section>
    </section>
  );
};

export default NotFound;

NotFound.defaultProps = {
  title: 'Title'
};
NotFound.propTypes = {
  title: PropTypes.string
};
