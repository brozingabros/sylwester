import React, { useState } from 'react';
import { ErrorMessage, Field, Formik, Form as FormikForm } from 'formik';
import * as Yup from 'yup';
import { SubmitButton } from '../Button';

const encode = data => {
  return Object.keys(data)
    .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(data[key])}`)
    .join('&');
};

const Form = () => {
  const [feedback, setFeedback] = useState(false);

  const FormValues = {
    name: '',
    email: '',
    message: ''
  };

  const FormSchema = Yup.object().shape({
    name: Yup.string().required('❗  This is a required field'),
    email: Yup.string().email('❗ Invalid email address').required('❗ Required'),
    message: Yup.string().required('❗ Required')
  });

  const handleFieldStyling = (formikProps, formikPropsKey) => {
    if (formikProps.touched[formikPropsKey] && formikProps.errors[formikPropsKey]) {
      return 'block border-4 border-error focus:shadow-outline outline-none p-2 text-black w-full';
    }
    if (formikProps.touched[formikPropsKey]) {
      return 'block border-4 border-success focus:shadow-outline outline-none p-2 text-black w-full';
    }

    return 'block border-4 focus:shadow-outline p-2 outline-none text-black w-full';
  };

  const handleSubmit = (values, { setSubmitting }) => {
    fetch('/', {
      method: 'POST',
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      body: encode({ 'form-name': 'contact', ...values })
    })
      .then(setSubmitting(true))
      .then(setFeedback(true));
  };

  return (
    <>
      {!feedback ? (
        <Formik initialValues={FormValues} validationSchema={FormSchema} onSubmit={handleSubmit}>
          {formikProps => (
            <FormikForm className='flex flex-col'>
              <input name='form-name' type='hidden' value='contact' />

              <label className='block font-body mb-8 text-primary' htmlFor='name'>
                Name
                <Field className={handleFieldStyling(formikProps, 'name')} name='name' type='text' />
                <ErrorMessage
                  className='bg-error-shade block font-body p-4 text-error text-sm w-full'
                  component='span'
                  name='name'
                />
              </label>

              <label className='block font-body mb-8 text-primary' htmlFor='email'>
                Email Address
                <Field className={handleFieldStyling(formikProps, 'email')} name='email' type='email' />
                <ErrorMessage
                  className='bg-error-shade block font-body p-4 text-error text-sm w-full'
                  component='span'
                  name='email'
                />
              </label>

              <label className='block font-body mb-8 text-primary' htmlFor='message'>
                Message
                <Field as='textarea' className={handleFieldStyling(formikProps, 'message')} name='message' rows={5} />
                <ErrorMessage
                  className='bg-error-shade block font-body p-4 text-error text-sm w-full'
                  component='span'
                  name='message'
                />
              </label>
              <SubmitButton />
            </FormikForm>
          )}
        </Formik>
      ) : (
        <section className='flex items-center justify-center'>
          <p className='border-4 border-success font-bold p-4 text-success text-xl'>
            Thank you for getting in touch!
            <span className='block font-body font-medium text-base'>
              I appreciate you contacting me. I will get back in touch with you soon!
            </span>
          </p>
        </section>
      )}
    </>
  );
};

export default Form;
