import React from 'react';
import PropTypes from 'prop-types';

const hamburgerState = {
  open: 'hamburger-menu open',
  close: 'hamburger-menu'
};

const Hamburger = ({ clickState, onClick }) => {
  return (
    <section>
      <button className={clickState ? hamburgerState.open : hamburgerState.close} type='button' onClick={onClick}>
        <div className='hamburger-menu-burger' />
      </button>
      <p
        className={
          clickState
            ? 'font-body relative text-center text-secondary text-xs z-30'
            : 'font-body relative text-center text-primary text-xs z-30'
        }>
        {clickState ? 'Close' : 'Menu'}
      </p>
    </section>
  );
};

export default Hamburger;

Hamburger.defaultProps = {
  clickState: false,
  onClick: false
};

Hamburger.propTypes = {
  clickState: PropTypes.bool,
  onClick: PropTypes.func
};
