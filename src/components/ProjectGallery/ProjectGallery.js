import React from 'react';
import { useRouteMatch } from 'react-router-dom';
import projectData from '../../constants/projects';
import { Button } from '../Button';

const ProjectGallery = () => {
  const { url } = useRouteMatch();
  return (
    <section className='flex flex-wrap justify-between w-full'>
      {projectData.map(project => {
        return (
          <section className='flex flex-col justify-between mb-16 md:w-1/2 px-2 w-full' key={project.id}>
            <section className='w-full'>
              <section
                className='block h-0 relative'
                style={{ paddingBottom: `${(project.img.height / project.img.width) * 100}%` }}>
                <img
                  alt={project.img.alt}
                  className='absolute block h-full left-0 top-0'
                  height={project.img.height}
                  sizes={project.img.sizes}
                  src={project.img.src}
                  srcSet={project.img.srcSet.map(set => `${project.img.src}?nf_resize=fit&w=${set.size} ${set.width}w`)}
                  width={project.img.width}
                />
              </section>
              <section className='mt-4'>
                <h3 className='font-primary leading-none text-lg text-primary uppercase'>{project.title}</h3>
                <h4 className='font-secondary leading-none mt-2 text-md text-primary uppercase'>{project.tagline}</h4>
                <p className='font-body leading-6 mt-8 text-base text-primary'>{project.description}</p>
              </section>
            </section>
            <section className='flex flex-col justify-between mt-8'>
              <section className='flex justify-end'>
                <Button url={`${url}/${project.url}`} />
              </section>
            </section>
          </section>
        );
      })}
    </section>
  );
};

export default ProjectGallery;
