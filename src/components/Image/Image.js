import React from 'react';
import 'lazysizes';
import 'lazysizes/plugins/attrchange/ls.attrchange';
import PropTypes from 'prop-types';
import { BREAKPOINT } from '../../constants';

export const GalleryImage = ({ source, lazyloadTarget, size, windowWidth, wrapperElement }) => {
  const WrapperElement = wrapperElement;

  const handleAspectRatio = () => {
    if (size === 'half' && windowWidth > BREAKPOINT.SM) {
      return { paddingBottom: `${(source.height / source.width) * 50}%` };
    }
    return { paddingBottom: `${(source.height / source.width) * 100}%` };
  };

  if (source.lazyload[lazyloadTarget]) {
    return (
      <WrapperElement
        className={size === 'half' ? 'block h-0 relative sm:w-1/2 w-full ' : 'block h-0 relative w-full'}
        style={handleAspectRatio()}>
        <img
          alt={source.alt}
          className='absolute block h-full lazyload left-0 p-2 top-0 w-full'
          data-expand='-5'
          data-sizes={source.sizes}
          data-src={`${source.src}?nf_resize=fit&w=${source.srcSize}`}
          data-srcset={source.srcSet.map(set => `${source.src}?nf_resize=fit&w=${set.size} ${set.width}w`)}
        />
      </WrapperElement>
    );
  }

  return (
    <WrapperElement className={size === 'half' ? 'sm:w-1/2 w-full' : 'w-full'}>
      <img
        alt={source.alt}
        className='p-2 w-full'
        sizes={source.sizes}
        src={`${source.src}?nf_resize=fit&w=${source.srcSize}`}
        srcSet={source.srcSet.map(set => `${source.src}?nf_resize=fit&w=${set.size} ${set.width}w`)}
      />
    </WrapperElement>
  );
};

export const HeroImage = ({ source, wrapperElement }) => {
  const WrapperElement = wrapperElement;

  return (
    <WrapperElement
      className='block h-0 relative w-full'
      style={{ paddingBottom: `${(source.height / source.width) * 100}%` }}>
      <img
        alt={source.alt}
        className='absolute block h-full left-0 top-0 w-full'
        height={source.height}
        sizes={source.sizes}
        src={`${source.src}?nf_resize=fit&w=${source.srcSize}`}
        srcSet={source.srcSet.map(set => `${source.src}?nf_resize=fit&w=${set.size} ${set.width}w`)}
        width={source.width}
      />
    </WrapperElement>
  );
};

GalleryImage.defaultProps = {
  source: {},
  lazyloadTarget: '',
  size: 'full',
  windowWidth: 0,
  wrapperElement: 'section'
};
HeroImage.defaultProps = {
  source: {},
  wrapperElement: 'section'
};

GalleryImage.propTypes = {
  source: PropTypes.shape({
    alt: PropTypes.string,
    dataSrcSet: PropTypes.arrayOf(
      PropTypes.shape({
        size: PropTypes.number,
        width: PropTypes.number
      })
    ),
    height: PropTypes.number,
    id: PropTypes.number,
    lazyload: PropTypes.shape({
      desktop: PropTypes.bool,
      tablet: PropTypes.bool,
      mobile: PropTypes.bool
    }),
    sizes: PropTypes.string,
    src: PropTypes.string,
    srcSet: PropTypes.arrayOf(
      PropTypes.shape({
        size: PropTypes.number,
        width: PropTypes.number
      })
    ),
    srcSize: PropTypes.string,
    width: PropTypes.number
  }),
  lazyloadTarget: PropTypes.string,
  size: PropTypes.string,
  windowWidth: PropTypes.number,
  wrapperElement: PropTypes.string
};
HeroImage.propTypes = {
  source: PropTypes.shape({
    alt: PropTypes.string,
    height: PropTypes.number,
    id: PropTypes.number,
    sizes: PropTypes.string,
    src: PropTypes.string,
    srcSet: PropTypes.arrayOf(
      PropTypes.shape({
        size: PropTypes.number,
        width: PropTypes.number
      })
    ),
    srcSize: PropTypes.string,
    width: PropTypes.number
  }),
  wrapperElement: PropTypes.string
};
