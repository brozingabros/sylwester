import React from 'react';
import PropTypes from 'prop-types';

import { Title } from '../Title';
import { GalleryImage } from '../Image';

const ProjectDetail = ({ children, imageSource, title, windowWidth }) => {
  document.title = title;
  return (
    <section className='flex flex-wrap lg:w-9/12 md:mt-0 mt-8 mx-auto w-11/12'>
      <Title>{children}</Title>
      <ul className='flex flex-wrap justify-between'>
        {imageSource.map(image => (
          <GalleryImage
            key={image.id}
            lazyloadTarget='mobile'
            size={image.size}
            source={image}
            windowWidth={windowWidth}
            wrapperElement='li'
          />
        ))}
      </ul>
    </section>
  );
};

export default ProjectDetail;

ProjectDetail.defaultProps = {
  children: 'Title',
  title: 'Title',
  imageSource: [],
  windowWidth: 0
};

ProjectDetail.propTypes = {
  children: PropTypes.string,
  title: PropTypes.string,
  imageSource: PropTypes.arrayOf(PropTypes.object),
  windowWidth: PropTypes.number
};
