import React from 'react';
import PropTypes from 'prop-types';
import { FiChevronRight } from 'react-icons/fi';
import { Link } from 'react-router-dom';

export const Button = ({ url }) => {
  return (
    <button
      className='bg-secondary font-primary relative text-sm text-secondary uppercase button-primary'
      type='button'>
      <Link className='block pb-2 pl-6 pr-10 pt-3' to={url}>
        Explore
      </Link>
      <FiChevronRight className='absolute arrow-icon m-auto top-0' size='1.25rem' />
    </button>
  );
};

export const SubmitButton = () => {
  return (
    <button
      className='bg-success button-primary focus:shadow-outline font-primary outline-none pb-2 pl-6 pr-10 pt-3 self-end relative text-sm text-black uppercase'
      type='submit'>
      Submit
      <FiChevronRight className='absolute arrow-icon m-auto top-0' size='1.25rem' />
    </button>
  );
};

Button.defaultProps = {
  url: ''
};

Button.propTypes = {
  url: PropTypes.string
};
