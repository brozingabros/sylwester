import React from 'react';
import { Link } from 'react-router-dom';

const Logo = () => {
  return (
    <section className='lg:mx-24 lg:order-2 mx-0 order-0'>
      <Link className='flex flex-col items-start lg:items-center' to='/'>
        <h1 className='font-primary leading-none lg:text-6xl md:text-5xl sm:text-4xl text-3xl text-primary uppercase'>
          Sylwester
        </h1>
        <p className='font-secondary leading-none md:text-sm text-primary text-xs'>shots by Sebastian Vuye</p>
      </Link>
    </section>
  );
};

export default Logo;
