import React, { Suspense, useEffect } from 'react';
import { Route, Switch, useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';
import ReactGA from 'react-ga';
import routes from '../../constants/routes';
import ScrollToTop from '../../utils/ScrollToTop';
import { TempSkeleton } from '../Skeleton';

// Init Google Analytics
ReactGA.initialize('UA-177308255-1');

const Main = ({ windowWidth }) => {
  const location = useLocation();
  useEffect(() => {
    ReactGA.pageview(location.pathname + location.search);
  }, [location]);

  return (
    <main className='main'>
      <Suspense fallback={<TempSkeleton />}>
        <ScrollToTop />
        <Switch>
          {routes.map(route => {
            return (
              <Route exact={route.exact} key={route.id} path={route.path}>
                {route.component({ windowWidth })}
              </Route>
            );
          })}
        </Switch>
      </Suspense>
    </main>
  );
};

export default Main;

Main.defaultProps = {
  windowWidth: 0
};
Main.propTypes = {
  windowWidth: PropTypes.number
};
