import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import Switch from 'react-switch';
import { FiSun, FiMoon } from 'react-icons/fi';
import { ThemeContext } from '../ThemeProvider/ThemeProvider';
import { BREAKPOINT } from '../../constants';

const switchStyle = {
  checked: {
    backgroundColor: '#393e46',
    icon: 'flex h-full justify-center items-center pl-2 text-2xl text-primary',
    handleColor: '#eeeeee'
  },
  handleDiameter: 20,
  unchecked: {
    backgroundColor: '#000000',
    icon: 'flex h-full justify-center items-center pl-2 text-2xl text-secondary',
    handleColor: '#eeeeee'
  },
  width: 60
};

const Toggle = ({ windowWidth }) => {
  const { theme, setTheme } = useContext(ThemeContext);
  const handleChange = checked => (checked ? setTheme('dark') : setTheme('light'));

  return (
    <section className={windowWidth >= BREAKPOINT.LG ? 'absolute right-0 pr-5 pt-5' : 'flex justify-center mt-8'}>
      <Switch
        aria-checked='false'
        aria-label='Change theme'
        checked={theme === 'dark'}
        checkedIcon={<FiSun className={switchStyle.checked.icon} />}
        handleDiameter={switchStyle.handleDiameter}
        offColor={switchStyle.unchecked.backgroundColor}
        offHandleColor={switchStyle.unchecked.handleColor}
        uncheckedIcon={<FiMoon className={switchStyle.unchecked.icon} />}
        width={switchStyle.width}
        onChange={handleChange}
        onColor={switchStyle.checked.backgroundColor}
        onHandleColor={switchStyle.checked.handleColor}
      />
    </section>
  );
};

export default Toggle;

Toggle.defaultProps = {
  windowWidth: 0
};
Toggle.propTypes = {
  windowWidth: PropTypes.number
};
