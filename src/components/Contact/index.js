import React from 'react';
import PropTypes from 'prop-types';
import Obfuscate from 'react-obfuscate';
import { FiFacebook, FiInstagram } from 'react-icons/fi';
import Form from '../Form';
import { Title } from '../Title';

const Contact = ({ title }) => {
  document.title = title;
  return (
    <section className='flex flex-wrap md:mt-0 mt-8 mx-auto sm:w-9/12 w-11/12'>
      <Title>Contact</Title>
      <section className='flex flex-col sm:flex-row w-full'>
        <section className='mb-16 sm:mb-0 sm:w-1/2 p-2 w-full'>
          <p>
            <span className='font-primary text-lg text-primary uppercase'>Sebastian Vuye</span>
          </p>
          <Obfuscate className='font-body text-primary' email='hi@sylwester.be' />
          <section className='flex items-center mt-4'>
            <a className='pr-2' href='https://www.facebook.com/sebavuye/' rel='noopener noreferrer' target='_blank'>
              <FiFacebook size='1.25rem' />
            </a>

            <a href='https://www.instagram.com/sebavuye/' rel='noopener noreferrer' target='_blank'>
              <FiInstagram size='1.25rem' />
            </a>
          </section>
        </section>
        <section className='sm:w-1/2 p-2 w-full'>
          <Form />
        </section>
      </section>
    </section>
  );
};

export default Contact;

Contact.defaultProps = {
  title: 'Title'
};
Contact.propTypes = {
  title: PropTypes.string
};
