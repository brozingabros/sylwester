import React, { createContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';

export const ThemeContext = createContext();

const getInitialTheme = () => {
  if (typeof window !== 'undefined' && window.localStorage) {
    const storedPreferences = window.localStorage.getItem('color-theme');
    if (typeof storedPreferences === 'string') {
      return storedPreferences;
    }
  }

  const userColorScheme = window.matchMedia('(prefers-color-scheme: light)');
  if (userColorScheme.matches) {
    return 'light';
  }

  return 'light';
};

const ThemeProvider = ({ initialTheme, children }) => {
  const [theme, setTheme] = useState(getInitialTheme);

  const handleSetTheme = colorScheme => {
    const rootHtmlElement = window.document.documentElement;
    rootHtmlElement.classList.remove(colorScheme === 'dark' ? 'light' : 'dark');
    rootHtmlElement.classList.add(colorScheme);
    localStorage.setItem('color-theme', colorScheme);
  };

  if (initialTheme) {
    handleSetTheme(initialTheme);
  }

  useEffect(() => {
    handleSetTheme(theme);
  }, [theme]);

  return <ThemeContext.Provider value={{ theme, setTheme }}>{children}</ThemeContext.Provider>;
};

export default ThemeProvider;

ThemeProvider.defaultProps = {
  initialTheme: '',
  children: <></>
};

ThemeProvider.propTypes = {
  initialTheme: PropTypes.string,
  children: PropTypes.node
};
