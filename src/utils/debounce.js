function debounce(func, duration) {
  let timeout;
  // eslint-disable-next-line func-names
  return function (...args) {
    const effect = () => {
      timeout = null;
      return func.apply(this, args);
    };
    clearTimeout(timeout);
    timeout = setTimeout(effect, duration);
  };
}

export default debounce;
